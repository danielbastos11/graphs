head.load(
	[
		'lib/require.js',
		'lib/jasmine/jasmine.js',
		'lib/jasmine/jasmine-html.js',
		'lib/jasmine/boot.js'
	],
	function(){
		require.config({
			paths: {
				graphs: './src',
				test: './test',
				lib: './lib'
			}
		});

		require(
			[
				'src/graphHandler',
				'test/graph'
			],
			function( GraphHandler ){
				window.onload();

				var graphHandler = new GraphHandler({
					vertices: ['v1', 'v2', 'v3', 'v4', 'v5'],
					edges: [
						['v1', 'v2'],
						['v1', 'v3'],
						['v1', 'v5'],
						['v5', 'v3'],
						['v2', 'v4'],
						['v3', 'v4']
					],
					graphType: 'adjList',
					isDirected: false
				});



				var graph = graphHandler.graph;
				graph.getVertices().forEach(function( elm ){
					console.log( elm + ': ' + graph.getSuccessors( elm ) );
				});

				console.log( graphHandler.hasEulerianPath() ?
								"Has an Eulerian Path" :
								"Doesn't have an Eulerian Path" );

				console.log( graph );
			}
		);
	}
);
