/**
* Created with graphs.
* User: danielbastos
* Date: 2015-01-29
* Time: 01:42 PM
* To change this template use Tools | Templates.
*/
define(
	[
		'src/adjList',
		'src/adjMatrix',
		'src/edgeList'
	],
	function( adjList, adjMatrix, edgeList ) {
		var graphImplementations = {
			'adjList': adjList,
			'adjMatrix': adjMatrix,
			'edgeList': edgeList
		};

		/**
		 * Implement graph algorithms
		 *
		 * @class GraphHandler
		 * @constructor
		 * @param {Object} [options] Startup options for the graph
		 * @param {String} [options.graphType='adjList'] Graph implementation to be used
		 */
		var GraphHandler = function( options ){
			if( !options  // Options isn't defined or
			   		|| !options.graphType  // Options is defined, but graphType isn't, or
			   		|| !graphImplementations
			   				.hasOwnProperty(options.graphType) ){ // graphType isn't valid

				graphType = 'adjList';
			}

			// Instantiate graph

			var isDirected = (options && options.isDirected);
			this.graph = new graphImplementations[options.graphType]( isDirected );

			// Insert vertices
			if( options && options.vertices ){
				this.graph.insertVertices( options.vertices );
			}

			// Insert edges

			var that = this;
			if( options && options.edges ){
				options.edges.forEach(function( element ){
					var vertex1 = element[0];
					var vertex2 = element[1];

					that.graph.insertEdge( vertex1, vertex2 );
				});
			}
		};


		GraphHandler.prototype.isSubgraph = function( graph ){
			var subgraphVertices = graph.getVertices();
			var graphVertices = this.graph.getVertices();

			var isSubGraph = true;

			while( subgraphVertices.length ){
				if( graphVertices.indexOf( subgraphVertices.pop() ) === -1 ){
					isSubGraph = false;
					break;
				}
			}

			return isSubGraph;
		}

		/**
		 * Verifies whether the graph has an Eulerian Path
		 *
		 * @return {Boolean} The answer to the question
		 */
		GraphHandler.prototype.hasEulerianPath = function(){
			var vertices = this.graph.getVertices();
			var evenCount = 0;

			while( vertices.length ){
				var adjancencyList = this.graph.adjacencyList( vertices.pop() );
				if( adjancencyList.length % 2 !== 0){
					evenCount++;
				} else if( adjancencyList.length === 0 ){
					return false;
				}

				if(evenCount > 2) return false;
			}

			return true;
		};

		/**
		 * Verifies whether the graph is connected
		 *
		 * @return {Boolean} The answer to the question
		 */
		GraphHandler.prototype.isConnected = function(){
			if(this.isDirected){
				throw new Error('Not implemented yet');
			}
			if(this.vertices.length === 0){
				return true;
			}

			var that = this;
			var testVertex = that.vertices[0];
			var connectedVertices = [testVertex];

			function getConnections( vertex ){
				var connections = that.adjacencyList( vertex );

				connections
					.forEach(function( element ){
						if( connectedVertices.indexOf( element ) === -1 ){

							connectedVertices.push( element );
							getConnections( element );
						}
					});
			}

			getConnections( testVertex );
							console.log(connectedVertices);

			if( connectedVertices.length == that.vertices.length )
				return true;
			return false;
		};

		return GraphHandler;
	}
);
