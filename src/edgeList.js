/*
 * Graph representation using Adjacency Matrixes
 *
 * Constraints:
 * 		- Every vertice has to be an String. Store content in your own dictionary if needed
 *
 * Created by Daniel Bastos
 * IFRN - 2015
 */

define(
	[
		'lib/lodash'
	],
	function( _ ){
		/**
		 * Graph
		 *
		 * @class Graph
		 * @constructor
		 *
		 * @param {Boolean} [isDirect=false] Tells whether the new graph is directed
		 */
		var Graph = function( isDirected ){
			this.edgeList = [];
			this.vertices = [];

			this.isDirect = !!isDirected;
		};

		/**
		 * Add a vertex to the graph
		 *
		 * @method insertVertex
		 * @param {Array} [vertices] Array of vertices to be added to the graph
		 * @param {String} [...vertex] Vertex to be added to the graph
		 */
		Graph.prototype.insertVertices = function( vertices ){
			if( !Array.isArray( vertices ) ){
				vertices = Array.prototype.slice.call( arguments );
			}

			var that = this;
			vertices.forEach(function( element ){
				that.vertices.push( element );
			});
		};

		/**
		 * Add an edge to the graph
		 *
		 * @method insertEdge
		 * @param {String} vertexI First vertex on the edge
		 * @param {String} vertexF Second vertex on the edge
		 */
		Graph.prototype.insertEdge = function( vertex1, vertex2 ){
			this.edgeList.push({
				start: vertex1,
				end: vertex2
			});
		};

		/**
		 * Remove an edge
		 *
		 * @method removeEdge
		 * @param {String} vertexI First vertex on the edge
		 * @param {String} vertexF Second vertex on the edge
		 */
		Graph.prototype.removeEdge = function(vertex1, vertex2){
			for( var i = 0; i < this.edgeList.length; i++ ){
				var edge = this.edgeList[i];

				if(
					// Vertices were provided in the right order
					( edge.start === vertex1 && edge.end === vertex2 ) ||
					// Vertices weren't provided in the right order,
					// but the graphs isn't directed, so who cares?
					( !this.isDirected &&
						( edge.start === vertex2 && edge.end === vertex1 ) )
				){
					this.edgeList.splice( i, 1 );
					return;
				}
			}
		};

		/**
		 * Remove a vertex and all its edges
		 * @method removeVertex
		 * @param {String} vertex Vertex to be removed
		 */

		Graph.prototype.removeVertex = function( vertex ){
			var vertexIndex = this.vertices.indexOf( vertex );

			if( vertexIndex === -1 ){
				throw new Error('Vertex has to have been added beforehand');
			}

			this.vertices.splice( vertexIndex, 1 );

			for( var i = 0; i < this.edgeList.length; i++ ){
				var edge = this.edgeList[i];

				if( edge.start === vertex || edge.end === vertex ){
					this.edgeList.splice( i, 1 );
				}
			}
		};

		/**
		 * List all the vertices of the graph
		 * @method vertices
		 * @return {Array} List of vertices on the graph
		 */
		Graph.prototype.getVertices = function(){
			// Returns a shallow copy of this.vertices
			return this.vertices.slice( 0 );
		};

		/**
		 * List all the vertices adjacent to a given vertex
		 * @method adjacencyList
		 * @param {String} vertex Vertex to which the returned vertices are adjacent
		 * @return {Array} List of vertices adjacent to vertex
		 */
		Graph.prototype.adjacencyList = function( vertex ){
			var adjacencyList = [];

			for( var i = 0; i < this.edgeList.length; i++ ){
				var edge = this.edgeList[i];

				if( edge.start === vertex ){
					adjacencyList.push(  edge.end  );
				} else if( edge.end === vertex ){
					adjacencyList.push( edge.start );
				}
			}

			return adjacencyList;
		};

		/**
		 * List all the predecessor of a given vertex
		 * @method getPredecessors
		 * @param {String} vertex Vertex of which the returned vertices are predecessors
		 * @return {Array} List of vertices that are predecessors to vertex
		 */
		Graph.prototype.getPredecessors = function( vertex ){
			var predecessors = [];

			for( var i = 0; i < this.edgeList.length; i++ ){
				var edge = this.edgeList[i];

				if( edge.end === vertex ){
					predecessors.push( edge.start );
				} else if( !this.isDirected && edge.start === vertex ){
					predecessors.push(  edge.end  );
				}
			}

			return predecessors;
		};

		/**
		 * List all the sucessors of a given vertex
		 * @method getSuccessors
		 * @param {String} vertex Vertex of which the returned vertices are successors
		 * @return {Array} List of vertices that are successors to vertex
		 */
		Graph.prototype.getSuccessors = function( vertex ){
			var successors = [];

			for( var i = 0; i < this.edgeList.length; i++ ){
				var edge = this.edgeList[i];

				if( edge.start === vertex ){
					successors.push( edge.end );
				} else if( !this.isDirected && edge.end === vertex ){
					successors.push(  edge.start  );
				}
			}

			return successors;
		};

		// Export the Graph constructor
		return Graph;
});
