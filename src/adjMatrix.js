/*
 * Graph representation using Adjacency Matrixes
 *
 * Constraints:
 * 		- Every vertice has to be an String. Store content in your own dictionary if needed
 *
 * Created by Daniel Bastos
 * IFRN - 2015
 */

define(
	[
		'lib/lodash'
	],
	function( _ ){
		/**
		 * Graph
		 *
		 * @class Graph
		 * @constructor
		 *
		 * @param {Boolean} [isDirected=false] Tells whether the new graph is directed
		 */
		var Graph = function( isDirected ){
			this.vertices		 = [];
			this.adjacencyMatrix = [];

			this.isDirected = isDirected || false;
		};

		/**
		 * Add a vertex to the graph
		 *
		 * @method insertVertex
		 * @param {Array} [vertices] Array of vertices to be added to the graph
		 * @param {String} [...vertex] Vertex to be added to the graph
		 */
		Graph.prototype.insertVertices = function( vertices ){
			if( !Array.isArray( vertices ) ){
				vertices = Array.prototype.slice.call( arguments );
			}

			while( vertices.length )
			{
				// Adds the new vertex to the list of vertices
				this.vertices.push( vertices.shift() );

				// Push a new col to the matrix
				this.adjacencyMatrix.push( [] );
			}


			// Pushes new 0s to every matrix until it has the new vertices computed
			// In practice, this adds a new column to every old row, and fills the new one with 0s.
			//
			var fixColumnSize = (function( size ){ // This function is made to pass 'size' to the internal one

				// This one does the actual job
				return function fixer( element ){
					while( element.length != size ){
						element.push( 0 );
					}
				};

			})(this.vertices.length);

			this.adjacencyMatrix.forEach( fixColumnSize );
		};

		/**
		 * Add an edge to the graph
		 *
		 * @method insertEdge
		 * @param {String} vertexI First vertex on the edge
		 * @param {String} vertexF Second vertex on the edge
		 */
		Graph.prototype.insertEdge = function( vertex1, vertex2 ){
			// Find indexes
			var vertex1Index = this.vertices.indexOf( vertex1 );
			var vertex2Index = this.vertices.indexOf( vertex2 );

			// Check for errors
			if( vertex1Index === -1 || vertex2Index === -1 ){
				throw new Error('The vertex has to have been added to the graph beforehand');
			}

			// Perform insertion
			this.adjacencyMatrix[vertex1Index][vertex2Index]++;

			if( !this.isDirected ){
				this.adjacencyMatrix[vertex2Index][vertex1Index]++;
			}
		};

		/**
		 * Remove an edge
		 *
		 * @method removeEdge
		 * @param {String} vertexI First vertex on the edge
		 * @param {String} vertexF Second vertex on the edge
		 */
		Graph.prototype.removeEdge = function(vertex1, vertex2){
			// Find indexes
			var vertex1Index = this.vertices.indexOf( vertex1 );
			var vertex2Index = this.vertices.indexOf( vertex2 );

			// Check for errors
			if( vertex1Index === -1 || vertex2Index === -1 ){
				throw new Error('The vertices have to have been added to the graph beforehand');
			}

			// Perform removal
			if(this.adjacencyMatrix[vertex1Index][vertex2Index] > 0)
				this.adjacencyMatrix[vertex1Index][vertex2Index]--;
		};

		/**
		 * Remove a vertex and all its edges
		 * @method removeVertex
		 * @param {String} vertex Vertex to be removed
		 */

		Graph.prototype.removeVertex = function( vertex ){
			var vertexIndex = this.vertices.indexOf( vertex );

			if( vertexIndex === -1 ){
				throw new Error('The vertex has to have been added to the graph beforehand');
			}

			// Remove the row corresponding to the vertex
			var removeCol = function( element ){
				element.splice( vertexIndex, 1 );
			};
			this.adjacencyMatrix.forEach( removeCol );

			// Remove the col corresponding to the vertex
			this.adjacencyMatrix.splice( vertexIndex, 1 );
		};

		/**
		 * List all the vertices of the graph
		 * @method vertices
		 * @return {Array} List of vertices on the graph
		 */
		Graph.prototype.getVertices = function(){
			// Returns a shallow copy of this.vertices
			return this.vertices.slice( 0 );
		};

		/**
		 * List all the predecessor of a given vertex
		 * @method getPredecessors
		 * @param {String} vertex Vertex of which the returned vertices are predecessors
		 * @return {Array} List of vertices that are predecessors to vertex
		 */
		Graph.prototype.getPredecessors = function( vertex ){
			var vertexIndex = this.vertices.indexOf( vertex );
			var that = this;

			if( vertexIndex === -1 ){
				throw new Error('The vertex has to have been added to the graph beforehand');
			}

			// Had to do it like this because JavaScript's lamba functions change the
			// "this" pointer to "window". The self-invoking function makes sure that
			// the inner function have plain access to "this.vertices"
			var adjList = [];

			var mapVertices = function( element, index ){
				if( element[vertexIndex] !== 0 ){
					adjList.push( that.vertices[index] );
				}
			};

			this.adjacencyMatrix
				.forEach( mapVertices );


			return adjList;

		};

		/**
		 * List all the sucessors of a given vertex
		 * @method getSuccessors
		 * @param {String} vertex Vertex of which the returned vertices are successors
		 * @return {Array} List of vertices that are successors to vertex
		 */
		Graph.prototype.getSuccessors = function( vertex ){
			var vertexIndex = this.vertices.indexOf( vertex );

			if( vertexIndex === -1 ){
				throw new Error('The vertex has to have been added to the graph beforehand');
			}

			var that = this;

			// Had to do it like this because JavaScript's lamba functions change the
			// "this" pointer to "window". The self-invoking function makes sure that
			// the inner function have plain access to "this.vertices"
			var adjList = [];

			var mapVertices = function( element, index ){
				if( element !== 0 ){
					adjList.push( that.vertices[index] );
				}
			};

			this.adjacencyMatrix[vertexIndex]
				.forEach( mapVertices );


			return adjList;
		};

		/**
		 * List all the vertices adjacent to a given vertex
		 * @method adjacencyList
		 * @param {String} vertex Vertex to which the returned vertices are adjacent
		 * @return {Array} List of vertices adjacent to vertex
		 */
		Graph.prototype.adjacencyList = function( vertex ){
			if( !this.isDirected ){
				return this.getSuccessors( vertex );
			} else {
				var successors = this.getSuccessors( vertex );
				var predecessors = this.getPredecessors( vertex );

				return _.union( successors, predecessors );
			}
		};

		return Graph;
});
