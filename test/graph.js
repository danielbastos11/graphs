define(
	[
		'graphs/adjList'
	],
	function( Graph ) {
		describe('Vertice insertion into graphs', function(){
			var myGraph = new Graph();

			// Define insertion lists
			var singleInsertion = ['v1', 'v2', 'v3'];
			var insertionByArray = ['v4', 'v5', 'v6'];
			var insertionByArgumentList = ['v7', 'v8', 'v9'];

			// Insert each array accordingly
			singleInsertion.forEach(function( element ){
				myGraph.insertVertices( element );
			});

			myGraph.insertVertices( insertionByArray.slice( 0 ) );

			myGraph.insertVertices.apply( myGraph, insertionByArgumentList );

			// Get vertices list

			var verticesList = myGraph.getVertices();

			// Run tests

			it('should insert vertices individually', function(){
				singleInsertion.forEach(function( vertex ){
					expect( verticesList ).toContain( vertex );
				});
			});

			it('should insert arrays of vertices', function(){
				insertionByArray.forEach(function( vertex ){
					expect( verticesList ).toContain( vertex );
				});
			});

			it('should insert vertices provided in a list of arguments', function(){
				insertionByArgumentList.forEach(function( vertex ){
					expect( verticesList ).toContain( vertex );
				});
			});
		});
		describe('Edge insertion into graphs', function(){
			//
			// Preparation
			//

			// Vertices
			var vertex = 'v5';
			var predecessors = ['v1', 'v2', 'v3', 'v4'];
			var successors = ['v6', 'v7', 'v8', 'v9', 'v10'];

			// Graphs
			var   directedGraph = new Graph( true  );
			var undirectedGraph = new Graph( false );

			// Vertex insertion

			  directedGraph.insertVertices( vertex );
			undirectedGraph.insertVertices( vertex );
			  directedGraph.insertVertices( predecessors.concat( successors ) );
			undirectedGraph.insertVertices( predecessors.concat( successors ) );

			// Edge insertion
			predecessors
				.forEach(function( element ){
					  directedGraph.insertEdge( element, vertex );
					undirectedGraph.insertEdge( element, vertex );
				});

			successors
				.forEach(function( element ){
					  directedGraph.insertEdge( vertex, element );
					undirectedGraph.insertEdge( vertex, element );
				});

			//
			// Tests
			//

			it("should add successors and predecessors", function(){
				var   directedSuccessors   =   directedGraph.getSuccessors( vertex );
				var undirectedSuccessors   = undirectedGraph.getSuccessors( vertex );
				var   directedPredecessors =   directedGraph.getPredecessors( vertex );
				var undirectedPredecessors = undirectedGraph.getPredecessors( vertex );

				successors.forEach(function( element ){
					expect(   directedSuccessors ).toContain( element );
					expect( undirectedSuccessors ).toContain( element );
				});

				predecessors.forEach(function( element ){
					expect(   directedPredecessors ).toContain( element );
					expect( undirectedPredecessors ).toContain( element );
				});
			});

			it("shouldn't add backwards in directed graphs", function(){
				var   directedGraphPredecessors   =   directedGraph.getPredecessors( vertex );
				var   directedGraphSuccessors 	  =   directedGraph.getSuccessors( vertex );

				successors.forEach(function( element ){
					expect(   directedGraphPredecessors ).not.toContain( element );
				});

				predecessors.forEach(function( element ){
					expect(   directedGraphSuccessors ).not.toContain( element );
				});
			});

			it("should add backwards in undirected graphs", function(){
				var undirectedGraphPredecessors   = undirectedGraph.getPredecessors( vertex );
				var undirectedGraphSuccessors 	  = undirectedGraph.getSuccessors( vertex );

				successors.forEach(function( element ){
					expect( undirectedGraphPredecessors ).toContain( element );
				});

				predecessors.forEach(function( element ){
					expect( undirectedGraphSuccessors ).toContain( element );
				});
			});
		});



	}
);
